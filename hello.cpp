#include <iostream>
#include <string>
#include "hello.h"
using namespace std;


#ifdef __cplusplus
extern "C" {
#endif

int add (int a, int b) {
    cout <<"add a: ";
    cout <<a;
    cout <<",b: ";
    cout <<b <<endl;
    cout <<"a+b: ";
    cout <<a+b<<endl;
    return a+b;
}

#ifdef __cplusplus
}
#endif