#include "myHello.h"
#include "hello.h"
#include <stdio.h>


int main() {

    printf("======C call cpp normal function.======\n");
    int ret = add(3, 4);
    printf("ret = %d\n", ret);
    printf("\n======C call cpp primitive function.======\n");

    struct hello* pHello = newHello();
    interSetName(pHello, "xiaoming");
    interSayHello(pHello);
    char name[256] = {0};
    interGetName(pHello, name, 256);
    printf("interGetName name:%s\n", name);

    return 0;
}