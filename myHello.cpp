#include "helloClass.h"
#include "myHello.h"
#include <string.h>

#ifdef __cplusplus
extern "C"{
#endif

hello* newHello() {
    return new hello();
};

void interSetName(hello *v, const char* p) {
    v->setName(p);
}

void interGetName(hello *v, char* pName, int len) {
    strcpy_s(pName, len, v->getName().c_str());
}

void interSayHello(hello *v) {
    v->sayHello();
}

#ifdef __cplusplus
}
#endif