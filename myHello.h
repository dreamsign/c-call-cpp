#ifndef MYHELLO_h
#define MYHELLO_h
 
 
#ifdef __cplusplus
extern "C"{
#endif
 
 
typedef struct hello hello;
hello* newHello();
void interSetName(hello *v,const char* p);
void interGetName(hello *v, char* pName, int len);
void interSayHello(hello *v);
 
 
#ifdef __cplusplus
}
#endif
#endif