#ifndef HELLOCLASS_H
#define HELLOCLASS_H
#include <string>
using namespace std;

class hello
{
public:
    hello(){};
    ~hello(){};
    void sayHello();    
    void setName(string name);
    string getName();
private:
    string mName;  
};
 
 
#endif