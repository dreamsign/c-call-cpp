#include "helloClass.h"
#include <iostream>
#include <string>
using namespace std;

void hello::sayHello() {
    cout << "hello " << mName << endl;
}

void hello::setName(string name) {
    mName = name;
}

string hello::getName() {
    return mName;
}